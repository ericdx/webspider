﻿using CrawlerLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp
{
    class Program
    {
        const string OUTFOLDER = "FetchedPages/";
        static void Main(string[] args)
        {
            try
            {
                var startUrl = ConsoleApp.Properties.Settings.Default.url;
                var depth = ConsoleApp.Properties.Settings.Default.hops;
                var crawler = new UrlCrawler(startUrl);
                var pages = crawler.ContinueFetch("/", depth);
                Directory.CreateDirectory(OUTFOLDER);
                foreach (var page in pages)
                {
                    if (page.fetchSuccess)
                        File.WriteAllText(OUTFOLDER + page.url.clearFilePath(), page.html);
                    else Console.WriteLine($"page {page.url} not fetched, error: {page.FailDescription}");
                }
            } catch (Exception ex)
            {
                Console.WriteLine($"Fatal error: {ex.Message}");
            }
        }
    }
}
