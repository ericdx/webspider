﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerLibrary
{
    public static class Extensions
    {
        public static string clearFilePath(this string inputLink)
        {
            return new string(inputLink.Where(x => char.IsLetterOrDigit(x) ).ToArray()) + ".htm";
        }
    }
}
