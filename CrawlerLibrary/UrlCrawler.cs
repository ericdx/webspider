﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrawlerLibrary
{
    public class UrlCrawler
    {
        string baseUrl { get; set; }
        public UrlCrawler(string _startUrl)
        {
            //detect base url for crawling all subsequent relative urls
           var urlReg = Regex.Match(_startUrl, @"(https?://)(.+)/?",RegexOptions.RightToLeft);
           var proto = urlReg.Groups[1].Value;
           var link = urlReg.Groups[2].Value;
            var slashPos = link.IndexOf('/');
            var domain = slashPos > 0 ? link.Substring(0,slashPos ) : link;
            baseUrl = proto + domain;
        }
        
        public List<Page> ContinueFetch(string nextUrl, int depth)
        {
            var allTree = new List<Page>();
            if (depth-- < 0) return allTree;
            var correctUrl = nextUrl;
            if (nextUrl.StartsWith("/"))
            {//case relative url
                correctUrl = baseUrl + nextUrl;
            }
            var nextPages = getPagesByUrls(new[] { correctUrl });
            allTree.AddRange(nextPages);
            foreach(var page in nextPages.Where(n => n.pageNextUrls != null))
            {
                foreach (var url in page.pageNextUrls)
                    allTree.AddRange(ContinueFetch(url, depth));            

            }
            return allTree;
        }
        public List<Page> getPagesByUrls(string[] urls)
        {
            var pages = new List<Page>();
            using (WebClient client = new WebClient())
            {
                foreach (var url in urls)
                {
                    var page = new Page { url = url, fetchSuccess = true };
                    try
                    {

                        string htmlCode = client.DownloadString(url); //"http://somesite.com/default.html"
                        page.html = htmlCode;
                        page.pageNextUrls = parseLinks(htmlCode);

                    }
                    catch (Exception ex)
                    {
                        page.FailDescription = ex.Message;
                        page.fetchSuccess = false;
                    }
                    pages.Add(page);
                }
            }
            return pages;
        }

        private string[] parseLinks(string htmlCode)
        {

            var mc = Regex.Matches(htmlCode, @"<.*?href=(""|')(.+?)(""|').*?>")
.OfType<Match>().ToArray();
           return mc.Select(m => m.Groups[2].Value).Distinct().ToArray();
                
            
        }
    }
}
