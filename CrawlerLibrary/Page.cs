﻿using System.Collections.Generic;

namespace CrawlerLibrary
{
    public class Page
    {
        public string url;
        public string html;
        public string[] pageNextUrls;//{ get; set; } = new string[] { };
      //  public List<Page> linkedPages;
        public bool fetchSuccess;
        public string FailDescription;
    }
}